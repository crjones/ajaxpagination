# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Lead

# Create your views here.
def index(request):
    page = request.GET.get('page',1)
    paginator = Paginator(Lead.objects.all(), 7)
    return render(request, 'leads/index.html', {'leads': paginator.page(1),'num_pages': range(1,paginator.num_pages+1),'end_index': paginator.num_pages} )

def find(request):
    leads = Lead.objects.filter(first_name__startswith=request.POST['name_starts_with']).filter(created_at__range=(request.POST['from_date'],request.POST['to_date'])).order_by('id') | Lead.objects.filter(last_name__startswith=request.POST['name_starts_with']).filter(created_at__range=(request.POST['from_date'],request.POST['to_date'])).order_by('id')

    paginator = Paginator(leads, 7)
    try:
        page = int(request.POST['page_number'])
        paginator.page(page).object_list
    except EmptyPage or PageNotAnInteger:
        page = 1

    return render(request, 'leads/table.html', {'end_index': paginator.num_pages,'leads': paginator.page(page).object_list,'num_pages':range(1,paginator.num_pages+1)})